## How to run application
# Run Kafka and other services
```shell
~ docker-compose up
```

Go to Control Center: http://localhost:9021/
# Run application
Produce messages: http://localhost:8080/produce
Consume messages: http://localhost:8080/consume

Consumed messages will be printed in console.
You also can view messages in control center.