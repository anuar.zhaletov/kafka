package com.kafka.services;

import com.kafka.schema.Weather;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Random;

@Service
public class ProducerService {
  private static final List<String> CITIES = List.of("The Hague", "Amsterdam", "Utrecht", "Rotterdam", "Eindhoven", "Almere", "Groningen", "Breda", "Apeldoorn", "Haarlem", "Zaanstad", "Arnhem", "’s-Hertogenbosch", "Leeuwarden", "Maastricht", "Zwolle", "Alphen aan den Rijn", "Emmen", "Delft", "Deventer", "Hilversum", "Heerlen", "Purmerend", "Lelystad", "Roosendaal", "Spijkenisse", "Ede", "Gouda", "Zaandam", "Bergen op Zoom", "Capelle aan den IJssel", "Veenendaal", "Katwijk", "Zeist", "Nieuwegein", "Assen", "Hardenberg", "Barneveld", "Roermond", "Heerhugowaard", "Oosterhout", "Den Helder", "Hoogeveen", "Kampen", "Woerden", "Houten", "Sittard", "IJmuiden", "Middelburg", "Harderwijk", "Zutphen", "Ridderkerk", "Kerkrade", "Veldhoven", "Medemblik", "Zwijndrecht", "Vlissingen", "Rheden", "Etten-Leur", "Zevenaar", "Venray", "Noordwijk", "Tiel", "Uden", "Huizen", "Beverwijk", "Wijchen", "Dronten", "Hellevoetsluis", "Maarssen", "Leidschendam", "Heemskerk", "Veghel", "Goes", "Venlo", "Landgraaf", "Teijlingen", "Geleen", "Hellendoorn", "Castricum", "Gorinchem", "IJsselstein", "Sneek", "Tynaarlo", "Maassluis", "Bussum", "Deurne", "Oldenzaal", "Aalsmeer", "Rosmalen", "Lonneker", "Hendrik-Ido-Ambacht", "Valkenswaard", "Boxtel", "Leusden", "Bergen", "Heesch", "Oosterend", "Krimpen aan den IJssel", "Diemen");
  private static final double MAX_TEMPERATURE = 40.0;
  private static final double MIN_TEMPERATURE = -20.0;

  @Autowired
  private KafkaTemplate<String, Weather> kafkaTemplate;

  public void produce() throws InterruptedException {
    while (true) {
      Weather weather = new Weather();
      weather.setCityName(getRandomCity());
      weather.setTemperature(getRandomTemperature());

      ProducerRecord<String, Weather> producerRecord = new ProducerRecord<>("Topic_Weather", weather.getCityName().toString(), weather);
      producerRecord.headers().add("client-id", "2334".getBytes(StandardCharsets.UTF_8));
      kafkaTemplate.send(producerRecord);
      Thread.sleep(500);
    }
  }

  private static String getRandomCity() {
    return CITIES.get(new Random().nextInt(CITIES.size()));
  }

  private static Double getRandomTemperature() {
    Random r = new Random();
    return MIN_TEMPERATURE + (MAX_TEMPERATURE - MIN_TEMPERATURE) * r.nextDouble();
  }
}