package com.kafka.controllers;

import com.kafka.services.ProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class KafkaController {
  @Autowired private ProducerService producerService;

  @GetMapping("/produce")
  public void producerAvroMessage() throws InterruptedException {
    producerService.produce();
  }
}
