package com.kafka.config;

import com.kafka.schema.Weather;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;

import java.util.Map;

@Configuration
public class KafkaProducerConfig {
  private final static Map<String, Object> PRODUCER_CONFID = Map.of(
      ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092",
      ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
      ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class,
      "schema.registry.url", "http://localhost:8081"
  );

  @Bean
  public NewTopic moviesTopic() {
    return new NewTopic("Topic_Weather", 1, (short) 1);
  }

  @Bean
  public ProducerFactory<String, Weather> producerFactory() {
    return new DefaultKafkaProducerFactory<>(PRODUCER_CONFID);
  }

  @Bean
  public KafkaTemplate<String, Weather> kafkaTemplate() {
    return new KafkaTemplate<>(producerFactory());
  }
}
