package com.kafka.listeners;

import lombok.extern.log4j.Log4j2;
import org.apache.avro.specific.SpecificRecordBase;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Kafka listener listens the all business contact publish events.
 */
@Log4j2
@Component
public class KafkaEventListener {
  private static final String PUBLISH_ID = "PB_IDENTITY";
  private static final String PUBLISH_MESSAGE = "PB_DATA_AREA";
  private static final int EVENT_NAME_START_INDEX = 0;
  private static final int EVENT_NAME_END_INDEX = 4;

  /**
   * The kafka listener.
   *
   * @param kafkaEvent listen the business contact events
   */
  @KafkaListener(topics = "${spring.kafka.consumer.topic-name}", groupId = "${spring.kafka.consumer.group-id}")
  public void kafkaEventListener(SpecificRecordBase kafkaEvent) {
    Optional.ofNullable(kafkaEvent)
        .ifPresentOrElse(e -> {
          System.out.println("cityName: " + e.get("cityName") + ", temperature: " + e.get("temperature"));
        }, () -> log.warn("The received event is empty"));
  }
}
